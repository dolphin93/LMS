package io.github.wx.common.druid;

import com.jfinal.plugin.druid.IDruidStatViewAuth;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jie on 2017/4/7.
 * druid 统计页面认证具体实现
 */
public class DruidStatViewAuthImpl implements IDruidStatViewAuth {
    public boolean isPermitted(HttpServletRequest request) {
        boolean isPermitted = false;
        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            isPermitted = subject.isPermitted("druid:manage");
        }
        return isPermitted;
    }
}
