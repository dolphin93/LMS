package io.github.wx.common.easyui;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jie on 2017/4/3.
 * easyui 返回的datagrid 的对象
 *
 * @param <E>
 */
public class DataGrid<E> implements Serializable {
    private long total;
    private List<E> rows = new ArrayList<E>();
    private List<E> footer;
    /**
     * 返回额外参数
     */
    private Map data;

    public DataGrid() {
        super();
    }

    public DataGrid(Page<E> page) {
        this.total = page.getTotalRow();
        this.rows = page.getList();
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<E> getRows() {
        return rows;
    }

    public void setRows(List<E> rows) {
        this.rows = rows;
    }

    public List<E> getFooter() {
        return footer;
    }

    public void setFooter(List<E> footer) {
        this.footer = footer;
    }

    public Map getData() {
        return data;
    }

    public void setData(Map data) {
        this.data = data;
    }

}