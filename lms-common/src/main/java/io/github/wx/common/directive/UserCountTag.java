package io.github.wx.common.directive;

import com.jfinal.plugin.redis.Redis;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.stat.Scope;
import io.github.wx.common.shiro.kit.ShiroSessionKit;

import java.io.Writer;
import java.util.Set;

/**
 * Created by jie on 2017/4/9.
 * 获取当前登录总人数的宏
 */
public class UserCountTag extends Directive {
    public void exec(Env env, Scope scope, Writer writer) {
        Set<String> keys = Redis.use().keys(ShiroSessionKit.USER_SESSION + "*");
        write(writer, String.valueOf(keys.size()));
    }
}
