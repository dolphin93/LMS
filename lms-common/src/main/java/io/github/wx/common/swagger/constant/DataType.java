package io.github.wx.common.swagger.constant;


public interface DataType {
    String INTEGER = "Integer";
    String LONG = "Long";
    String STRING = "String";
}
