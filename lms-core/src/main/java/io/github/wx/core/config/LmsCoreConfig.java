package io.github.wx.core.config;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.github.jieblog.plugin.shiro.core.ShiroInterceptor;
import com.github.jieblog.plugin.shiro.core.ShiroPlugin;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;
import io.github.wx.common.directive.UserCountTag;
import io.github.wx.common.druid.DruidStatViewAuthImpl;
import io.github.wx.core.route.AdminRouts;
import io.github.wx.core.route.BizRoutes;
import io.github.wx.dao.model._MappingKit;
import net.dreamlu.ui.jfinal.AssetsDirective;

/**
 * Created by jie on 2017/4/1.
 * 框架的核心配置
 */
public class LmsCoreConfig extends JFinalConfig {
    private static Prop p = loadConfig();
    private WallFilter wallFilter;
    private Engine engine;

    private static Prop loadConfig() {
        try {
            return PropKit.use("profile.dev.properties");
        } catch (Exception e) {
            return PropKit.use("profile.sit.properties");
        }
    }

    /**
     * 配置全局常量
     *
     * @param me Constants
     */
    public void configConstant(Constants me) {
        me.setDevMode(p.getBoolean("devMode", false));
        me.setJsonFactory(new FastJsonFactory());
        me.setBaseUploadPath(p.get("uploadPath"));
    }

    /**
     * 配置路由
     *
     * @param me Routes
     */
    public void configRoute(Routes me) {
        me.add(new BizRoutes());
        me.add(new AdminRouts());
    }

    /**
     * 配置模板引擎
     *
     * @param me Engine
     */
    public void configEngine(Engine me) {
        this.engine = me;
        me.addDirective("assets", new AssetsDirective());
        me.addDirective("userCount", new UserCountTag());
    }

    /**
     * 配置插件
     *
     * @param me Plugins
     */
    public void configPlugin(Plugins me) {
        RedisPlugin redisPlugin = new RedisPlugin(p.get("redisCacheName"), p.get("redisHost"), p.get("redisPwd"));
        me.add(redisPlugin);
        ShiroPlugin shiroPlugin = new ShiroPlugin(engine);
        shiroPlugin.setLoginUrl("/login");
        shiroPlugin.setUnauthorizedUrl("/unauthorized");
        me.add(shiroPlugin);

        DruidPlugin druidPlugin = getDruidPlugin();
        wallFilter = new WallFilter();
        wallFilter.setDbType("mysql");
        druidPlugin.addFilter(wallFilter);
        druidPlugin.addFilter(new StatFilter());
        me.add(druidPlugin);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setShowSql(p.getBoolean("devMode", false));
        _MappingKit.mapping(arp);
        me.add(arp);
    }

    /**
     * 创建DruidPlugin 抽成公告方法，方便生成model时候调用
     *
     * @return DruidPlugin
     */
    static DruidPlugin getDruidPlugin() {
        return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
    }

    /**
     * 配置拦截器
     *
     * @param me Interceptors
     */
    public void configInterceptor(Interceptors me) {
        me.add(new ShiroInterceptor());
    }

    /**
     * 配置自定义处理器
     *
     * @param me Handlers
     */
    public void configHandler(Handlers me) {
        me.add(new DruidStatViewHandler("druid", new DruidStatViewAuthImpl()));
        me.add(new ContextPathHandler("ctx"));
    }

    /**
     * 启动后回调
     */
    public void afterJFinalStart() {
        wallFilter.getConfig().setSelectUnionCheck(false);
        engine.addSharedFunction("_common/common.html");
    }

    /**
     * 程序启动入口
     * <p>
     * IDEA 启动设置
     * JFinal.start("lms-web/src/main/webapp", 80, "/");
     * eclipse 启动设置 (/开头)
     * JFinal.start("/C:/workspace/LMS/lms-web/src/main/webapp/lms-web/src/main/webapp", 80, "/",10);
     *
     * @param args Args
     */
    public static void main(String[] args) {
        JFinal.start("lms-web/src/main/webapp", 80, "/");
    }
}
