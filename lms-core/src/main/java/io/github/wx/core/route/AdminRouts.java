package io.github.wx.core.route;

import com.jfinal.config.Routes;
import io.github.wx.common.swagger.controller.SwaggerController;
import io.github.wx.web.sys.IndexController;
import io.github.wx.web.sys.SysRoleMenuController;
import io.github.wx.web.sys.SysUserController;

/**
 * Created by jie on 2017/4/2.
 * 系统控制相关路由
 */
public class AdminRouts extends Routes {
    public void config() {
        setBaseViewPath("/WEB-INF/views");

        add("/", IndexController.class);
        add("/swagger", SwaggerController.class);
        add("/admin/user", SysUserController.class, "/admin/user");
        add("/admin/roleMenu", SysRoleMenuController.class, "/admin");
    }
}
