package io.github.wx.core.config;

import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

import javax.sql.DataSource;

/**
 * 由表到model 的反向生成工具
 */
public class GeneratorModel {

    /**
     * 重用 JFinalClubConfig 中的数据源配置，避免冗余配置
     */
    public static DataSource getDataSource() {
        DruidPlugin druidPlugin = LmsCoreConfig.getDruidPlugin();
        druidPlugin.start();
        return druidPlugin.getDataSource();
    }

    public static void main(String[] args) {
        // base model 所使用的包名
        String baseModelPackageName = "io.github.wx.dao.model.base";
        // base model 文件保存路径
        String baseModelOutputDir = "d:/jfinal/base";

        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "io.github.wx.dao.model" ;
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = "d:/jfinal/";

        // 创建生成器
        Generator gernerator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
        // 设置数据库方言
        gernerator.setDialect(new MysqlDialect());
        // 设置是否在 Model 中生成 dao 对象
        gernerator.setGenerateDaoInModel(true);
        // 设置是否生成字典文件
        gernerator.setGenerateDataDictionary(false);
        // 生成
        gernerator.generate();
    }
}




