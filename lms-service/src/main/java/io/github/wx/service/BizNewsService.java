package io.github.wx.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import io.github.wx.common.easyui.DataGrid;
import io.github.wx.dao.model.BizNews;

/**
 * Created by jie on 2017/4/19
 * BizNewsService.
 */
public class BizNewsService {
    public DataGrid getNewsPage(Integer page, Integer rows) {
        SqlPara sqlPara = BizNews.dao.getSqlPara("bizNews.findBizNewsPage");
        Page<BizNews> bizNewsPage = BizNews.dao.paginate(page, rows, sqlPara);
        return new DataGrid(bizNewsPage);
    }
}
