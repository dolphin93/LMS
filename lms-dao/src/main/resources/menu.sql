#namespace("menu")
#sql("findByRoleId")
SELECT
menu.menu_id,
menu.menu_name,
menu.menu_desc,
menu.url,
menu.permission,
menu.parent_id,
menu.sort,
menu.opened,
menu.`status`,
menu.type,
menu.create_time,
menu.update_time,
menu.del_flag
FROM
sys_menu AS menu
INNER JOIN sys_role_menu AS rm ON menu.menu_id = rm.menu_id
INNER JOIN sys_role ON rm.role_id = sys_role.role_id
WHERE
sys_role.role_id = #para(0)
AND
menu.del_flag = #para(1)
#end

#sql("findAllMenu")
SELECT
	*
FROM
	sys_menu
WHERE
	del_flag = #para(0)
ORDER BY
	sort ASC
#end

#sql("delMenu")
UPDATE sys_menu
SET del_flag = #para(0)
WHERE
	parent_id = #para(1)
#end

#sql("findUseMenuListByUserName")
SELECT
sys_menu.*,
sys_user.user_id
FROM
sys_menu
INNER JOIN sys_role_menu ON sys_role_menu.menu_id = sys_menu.menu_id
INNER JOIN sys_role ON sys_role_menu.role_id = sys_role.role_id
INNER JOIN sys_user_role ON sys_user_role.role_id = sys_role.role_id
INNER JOIN sys_user ON sys_user_role.user_id = sys_user.user_id
WHERE
sys_menu.type = 0 AND
sys_user.username = #para(0)
#end

#end