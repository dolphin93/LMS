#namespace("bizNews")

#sql("findBizNewsPage")
SELECT
	biz_news.id,
	biz_news.site_name,
	biz_news.author_name,
	biz_news.url,
	biz_news.title,
	biz_news.summary,
	biz_news.publish_date,
	biz_news.create_time
FROM
	biz_news
ORDER BY
	biz_news.create_time DESC
#end

#end