/**
 * Created by jie on 2017/4/8.
 */
var loginUrl = window.location.href;
if (loginUrl.indexOf("login") < 0){//是session过期的
    window.location.href = basePath;
}
function login() {
    var url = basePath + "/userLogin";
    var data = $("#loginForm").serialize();
    if (!$("#loginForm").valid()) {
        return;
    }
    $.post(url, data, function (response) {
        if (response.result) {
            window.location.href = basePath + "/index";
        } else {
            $("#errorMsg").empty();
            $("#errorMsg").append(response.msg);
        }
    }, 'json');
}

function enterlogin() {
    if (event.keyCode == 13) {
        event.returnValue = false;
        event.cancel = true;
        login();
    }
}

$(function () {
    // 判断时候在Iframe框架内,在则刷新父页面
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 5
            },
            password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            username: {
                required: "请输入用户名",
                minlength: "用户名长度不能小于 5 个字母"
            },
            password: {
                required: "请输入密码",
                minlength: "密码长度不能小于 5 个字母"
            }
        }
    })
})
