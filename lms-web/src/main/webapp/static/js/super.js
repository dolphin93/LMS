/*easyui样式初始化*/
$.fn.tabs.defaults.tabHeight = 32; //tab标签条高度
$.fn.linkbutton.defaults.height = 32; //按钮高度
$.fn.menu.defaults.itemHeight = 28; //menu高度

$.fn.validatebox.defaults.height = 32;
$.fn.textbox.defaults.height = 32;
$.fn.textbox.defaults.iconWidth = 24;
$.fn.datebox.defaults.height = 32;
$.fn.numberspinner.defaults.height = 32;
$.fn.timespinner.defaults.height = 32;
$.fn.numberbox.defaults.height = 32;
$.fn.combobox.defaults.height = 32;
$.fn.passwordbox.defaults.height = 32;
$.fn.filebox.defaults.height = 32;

$.fn.menu.defaults.noline = true
$.fn.progressbar.defaults.height = 18; //进度条

/**
 * 空函数
 */
function emptyFuntion() {
}


$(function () {
    /*设置按钮的下拉菜单*/
    $('.super-setting-icon').on('click', function () {
        $('#mm').menu('show', {
            top: 50,
            left: document.body.scrollWidth - 130
        });
    });

    /*修改主题*/
    $('#themeSetting').on('click', function () {
        var themeWin = $('#win').dialog({
            width: 460,
            height: 260,
            modal: true,
            title: '主题设置',
            buttons: [{
                text: '保存',
                id: 'btn-sure',
                handler: function () {
                    themeWin.panel('close');
                }
            }, {
                text: '关闭',
                handler: function () {
                    themeWin.panel('close');
                }
            }],
            onOpen: function () {
                $(".themeItem").show();
            }
        });
    });
    $(".themeItem ul li").on('click', function () {
        $(".themeItem ul li").removeClass('themeActive');
        $(this).addClass('themeActive');
    });

    /*退出系统*/
    $("#logout").on('click', function () {
        $.messager.confirm('提示', '确定退出系统？', function (r) {
            if (r) {
                window.location.href = basePath + "/loginOut";
            }
        });
    });
});
$.parser.onComplete = function () {
    $("#index").css('opacity', 1);
}

/**
 * 初始化示例
 */
function initDemo() {
    /*初始化示例div*/
    var demoPanelId = 'demoPanel' + (new Date()).getTime();
    $('#demoPanel').attr('id', demoPanelId);
    var demoPaneCodeId = 'demoPanelCode' + (new Date()).getTime();
    $('#demoPanelCode').attr('id', demoPaneCodeId);

    /*示例导航选中样式*/
    $(".demo-list>ul>li").on('click', function () {
        $('#et-demo').tabs('select', '预览');

        $(this).siblings().removeClass('super-accordion-selected');
        $(this).addClass('super-accordion-selected');
        //加载页面
        $('#' + demoPanelId).panel('open').panel('refresh', $(this).data('url'));
    });
}

/* 生成标签内容 */
function createTabContent(url) {
    return '<iframe style="width:100%;height:99%;" scrolling="auto" frameborder="0" src="' + url + '" name="center" allowfullscreen="true"></iframe>';
}


/**
 * href方式加载数据的特点：
 *被加载的页面只有body元素内部的内容才会被加载，也就是jQuery的ajax请求的只是html片段。
 *加载远程url时有遮罩效果，也就是“等待中……”效果，用户体验较好。
 *当加载的页面布局较为复杂，或者有较多的js脚本需要运行的时候，编码往往就需要谨慎了，容易出问题，后面会详细谈。
 *content方式加载数据的特点：
 *比较灵活，你可以在脚本里面拼写html代码，然后赋值给tab的content属性，不过这种写法会使得代码易读性变差。
 *可以把iframe赋给content，把一个iframe嵌入也就没有什么不能完成的了。
 *使用iframe会造成客户端js重复加载，浪费资源，比如说你主页面要引用easyui的库，你的iframe也要引用，浪费就产生了。
 */
function nodeClick(node) {
    //新增一个选项卡
    var tabUrl = node.attributes.url;
    var tabTitle = node.text;
    var type = node.attributes.openType;
    if (tabUrl == null || tabUrl == "") {
        return;
    }
    //tab是否存在
    if ($("#tt").tabs('exists', tabTitle)) {
        $("#tt").tabs('select', tabTitle);
    } else {
        console.log(type);
        console.log(tabUrl);
        if ("1" == type) {//
            $("#tt").tabs('add', {
                title: tabTitle,
                content: createTabContent(tabUrl),
                closable: true,
                width: $("#tt").parent().width(),
                height: $("#tt").parent().height()
            });
        } else {
            $('#tt').tabs('add', {
                title: tabTitle,
                href: tabUrl,
                closable: true
            });
        }
    }
}

/**
 * 对指定区域全屏
 * @param divEle   区域
 */
function fullscreen(divEle) {
    var requestMethod = divEle.requestFullScreen || divEle.webkitRequestFullScreen || divEle.mozRequestFullScreen || divEle.msRequestFullScreen;
    if (requestMethod) {
        requestMethod.call(divEle);
    } else if (typeof window.ActiveXObject !== "undefined") {
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}